#!/bin/bash

awk -F"\t" '{
	if($3 == ""){print $0}else{for(i=1; i<=8;i++){printf $i"\t"};};
	if($3 == "gene"){
		gene++
		gid=gene 
		while(length(gid)<5){gid="0"gid};
		gid="Sha_"gid
		print "ID="gid
		mrna=0
	};

	if($3 == "mRNA"){
		mrna++
		mid=gid"."mrna
		print "ID="mid";Parent="gid
		exon=0
		tutr=0
		futr=0
	};

	if($3 == "exon"){
		exon++
		eid=mid".exon"exon
		print "ID="eid";Parent="mid

	};


	if($3 == "CDS"){
		print "ID=cds."mid";Parent="mid
	};


	if($3 == "five_prime_UTR"){
		futr++
		print "ID="mid".utr5p"futr";Parent="mid
	};

	if($3 == "three_prime_UTR"){
		tutr++
		print "ID="mid".utr3p"tutr";Parent="mid
	};
}' $1
